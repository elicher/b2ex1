import React from 'react'

class Child extends React.Component{
    
  render(){
    return (

      <p>hello,{this.props.name}</p>
    )
}
}
export default Child
import React from 'react';
import Child from './components/Child'

class App extends React.Component {
  render(){
    let myName="Liza"
    return (
      <div>
         <Child name={myName}/>
      </div>
    )
  }
}
export default App;